const zulip = require('zulip-js');
const path = require('path');
const logger = require('./logger').get('main');
const dice = require('./quantum_dice');

const zuliprc = path.resolve(__dirname, "zuliprc");

const diceIdentifier = /(\d+)d(\d+)\s*(\+\d+|-\d+)?\s*([\w\s]+)?/ //https://regex101.com/r/fLxF8B/1
zulip({zuliprc}).then(client => {
    logger.info("Connection established");
    const handleEvent = (event)=>{
        if(event.type == "message"){
            var text = event.message.content.replace('@**Dice roller**', '').trim();
            logger.info(`Received message: ${text}`);
            var groups = text.match(diceIdentifier);
            if(groups){
                var number = parseInt(groups[1]);
                var type = parseInt(groups[2]);
                var modifier = 0;
                if(groups[3]){
                    modifier = parseInt(groups[3])
                }
                var waiting = []
                logger.info(`Rolling ${number} d${type}s`);
                for (var i = 0; i < number; i++){
                    waiting.push(dice(type));
                }

                Promise.all(waiting).then(results => {
                    var total = results.reduce((t,x) => t +x) + modifier;
                    var message = `**${event.message.sender_full_name} `
                    var roll_description = `${number}d${type}`;

                    if(modifier > 0){
                        roll_description += `+${modifier}`;
                    }else if(modifier < 0){
                        roll_description += modifier;
                    }

                    if(groups[4]){
                        message += `${groups[4]} (${roll_description})`;
                    }else{
                        message += `rolled ${roll_description}`
                    }

                    message += `**\n\n${total} (${results.toString()})`
                    client.messages.send({
                        type: "stream",
                        to: event.message.display_recipient,
                        subject: event.message.subject,
                        content: message
                    }).then(logger.info);
                }).catch(error => {
                    logger.error(`Unable to get dice rolls: ${error}`);
                    client.messages.send({
                        type: "stream",
                        to: event.message.display_recipient,
                        subject: event.message.subject,
                        content : `Something went wrong with the die roll, please try again or contact the server administrator`
                    }).then(logger.info);
                })

            }else{
                logger.error(`Received invalid message: ${event.message.content}`);
                client.messages.send({
                    type: "stream",
                    to: event.message.display_recipient,
                    subject: event.message.subject,
                    content: `"${text}" is not a valid dice format`
                }).then(logger.info);
            }
        }
    }

    client.callOnEachEvent(handleEvent, ['message']);
});