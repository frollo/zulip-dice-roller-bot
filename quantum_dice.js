const axios = require('axios').default;
const logger = require('./logger').get('dice');

var cache = []

function getNumber(size){
   var n1 = cache.pop();
   var n2 = cache.pop();

   return ((n1 * n2) % size) + 1;
}

function nextRoll(size){
    return new Promise((resolve, reject) => {
        if(cache.length > 2){
            logger.info(`Using the cache (there are ${cache.length} elements)`);
            resolve(getNumber(size));
        }else{
            logger.info("Reloading cache");
            axios.get("https://qrng.anu.edu.au/API/jsonI.php?length=1024&type=uint8")
                .then((response) => {
                    cache = response.data.data;
                    resolve(getNumber(size));
                })
                .catch(exception => {
                    reject(exception);
                })
        }
    });
}

module.exports = nextRoll;