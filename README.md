# Zulip dice roller bot

## Installation

Clone this repository and run `npm install`. You'll need to place a file called `zuliprc` in this folder in order for the bot to work and connect to your server. To obtain the `zuliprc` file, [add a bot](https://zulip.com/help/add-a-bot-or-integration) to your server (use "Generic bot" as a type) and then download the `zuliprc` file.

## Running

Zulip does not queue messages and thus you'll need your bot to always be online in order for it to work. Zulip's solution is to bundle everything along with Zulip itself, but I really dislike it.

In my experience, `supervisord` works really well for this use case.