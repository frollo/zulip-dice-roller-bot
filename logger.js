const winston = require('winston');

const consoleFormat = winston.format.printf(({ level, message,label, timestamp }) => {
    if (typeof message === 'object' && message != null){
        return `${timestamp} [${level}] (${label}): ${JSON.stringify(message)}`;
    }
    return `${timestamp} [${level}] (${label}): ${message}`;
  });

const container = new winston.Container();

container.add('main', {
    format: winston.format.combine(
        winston.format.label({label: 'main'}),
        winston.format.timestamp(),
        consoleFormat
    ),
    transports : [
        new winston.transports.Console()
    ]
});

container.add('dice', {
    format: winston.format.combine(
        winston.format.label({label: 'dice'}),
        winston.format.timestamp(),
        consoleFormat
    ),
    transports : [
        new winston.transports.Console()
    ]
});

module.exports = container;
